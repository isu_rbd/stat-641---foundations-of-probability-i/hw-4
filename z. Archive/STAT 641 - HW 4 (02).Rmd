---
title: "STAT 641 - HW 4"
author: "Ricardo Batista"
date: "9/28/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Problem 2.1

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\bigcup_{\alpha \in I} T^{-1}(A_\alpha) \subset T^{-1} \left( \bigcup_{\alpha \in I} A_\alpha\right)}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $\omega_1 \in \bigcup_{\alpha \in I} T^{-1}(A_\alpha)$. Then

$$
\omega_1 \in T^{-1}(A_\alpha) = \left\{\omega_1 : T(\omega_1) \in A_\alpha \right\}
$$

for some $\alpha \in I$. Since $T(\omega_1) \in A_{\alpha}$, then $T(\omega_1) \in \bigcup_{\alpha \in I} A_\alpha$. Then since 

$$
T^{-1}\left(\bigcup_{\alpha \in I} A_\alpha\right) 
= \left\{\omega_1 : T(\omega_1) \in \bigcup_{\alpha \in I} A_\alpha\right\}
= \left\{\omega_1 : T(\omega_1) \in A_\alpha \text{ for some } \alpha \in I\right\},
$$

we have that $\omega_1 \in T^{-1} \left( \bigcup_{\alpha \in I} A_\alpha\right)$.  


&nbsp;&nbsp;&nbsp;&nbsp; $\underline{T^{-1} \left( \bigcup_{\alpha \in I} A_\alpha\right) \subset \bigcup_{\alpha \in I} T^{-1}(A_\alpha)}$

&nbsp;&nbsp;&nbsp;&nbsp; Let $\omega_1 \in T^{-1} \left( \bigcup_{\alpha \in I} A_\alpha\right)$. That is,

$$
\omega_1 \in T^{-1} \left( \bigcup_{\alpha \in I} A_\alpha\right) 
= \left\{\omega_1 : T(\omega_1) \in \bigcup_{\alpha \in I} A_\alpha \right\}
= \left\{\omega_1 : T(\omega_1) \in A_\alpha \text{ for some } \alpha \in I\right\}.
$$

Then, $T(\omega_1) \in A_\alpha$ for some $\alpha$ and therefore

$$
\omega_1 \in \left\{\omega_1 : T(\omega_1) \in A_\alpha\right\} = T^{-1}(A_\alpha) \subset \bigcup_{\alpha \in I} T^{-1}(A_\alpha).  
$$


# Problem 2.3

&nbsp;&nbsp;&nbsp;&nbsp; Note that 

$$
I(x) := 
\begin{cases}
1 && \text{if } x \in \mathbb{R} \setminus 0\\
0 && \text{if } x = 0.
\end{cases}
$$

Therefore, for $E \in \mathcal{B}(\mathbb{R})$

$$
I^{-1}(E) = \left\{ x : I(x) \in E\right\} = 
\begin{cases}
\emptyset               && \text{if } 0, 1 \notin E\\
\mathbb{R} \setminus 0  && \text{if } 1 \in E, 0 \notin E\\
0                       && \text{if } 0 \in E, 1 \notin E\\
\mathbb{R}              && \text{if } 0, 1 \in E.
\end{cases}
$$

So $I$ is $\langle \mathcal{B}(\mathbb{R}), \mathcal{B}(\mathbb{R}) \rangle$-measurable since $\emptyset, \{0\}, \{1\}, \{0, 1\} \in \mathcal{B}(\mathbb{R})$. Then, since $g$ is $\langle \mathcal{F}, \mathcal{B}(\mathbb{R}) \rangle$-measurable, $I(g(w))$ is $\langle \mathcal{F}, \mathcal{B}(\mathbb{R}) \rangle$-measurable.
Let $W = \mathbb{R} \setminus 0$. Then let 

$$
I_W(x) := 
\begin{cases}
1 && \text{if }
\end{cases}
$$



Note that $\{0\}, \mathbb{R} \setminus 0\in \mathcal{B}(\mathbb{R})$, therefore

$$
\begin{aligned}
g^{-1}\left(\{0\}\right)
&= \left\{ \omega : g(\omega) \in \{0\} \right\}
= \left\{ \omega : g(\omega) = 0 \right\} \in \mathcal{F}\\[10pt]
g^{-1}\left(\mathbb{R} \setminus 0\right)
&= \left\{ \omega : g(\omega) \in \mathbb{R} \setminus 0 \right\}
= \left\{ \omega : g(\omega) \neq 0 \right\} \in \mathcal{F}
\end{aligned}
$$

since $\{0\} \in \mathcal{B}(\mathbb{R})$

$$
W = \left\{\omega : g(\omega) = 0\right\}
$$



We're given that $g$ is $\langle \mathcal{F}, \mathcal{B}(\mathbb{R}) \rangle$-measurable. Now let $I_W: \mathbb{R} \rightarrow \{0, 1\}$ $W \subset \mathbb{R}$ as a function  defined as

$$
I_W(x) := 
\begin{cases}
1 && \text{if } x \in W\\
0 && \text{if } x \notin W.
\end{cases}
$$

Then, note that for any set $E \in \mathcal{B}(\mathbb{R})$,



Let $V \equiv \left\{ \emptyset, \{0\}, \{1\}, \{0, 1\}\right\}$. Then

Recall the definition of the indicator function:





$I: \mathbb{R} \rightarrow \{0, 1\}$ be 
Note that for every $E \in \mathcal{B}(\mathbb{R})$, 

$$

$$


$$
I^{-1}(E) = \left\{ \omega : g(\omega) = 1 \right\}
$$


Let $E = \{ 0\}$. Then, since $g$ is $\langle \mathcal{F}, \mathcal{B}(\mathbb{R}) \rangle$-measurable

$$
g^{-1}(E) = \left\{ \omega : g(\omega) = 1 \right\}
$$



Let $\psi(x) = \frac{1}{x}$ for $x \in \mathbb{R} \setminus 0$. Since $\psi: \mathbb{R}\setminus 0 \rightarrow \mathbb{R}$ is continuous, then

$$
\xi \equiv \psi \circ g = \frac{1}{g(\omega)}
$$

is $\langle \mathcal{F}, \mathcal{B}(\mathbb{R}) \rangle$-measurable. Therefore,

$$
h \equiv 
$$


So $\psi$ is continuous.

So $\psi: \mathbb{R}\setminus 0 \rightarrow \mathbb{R}$ is continuous. 

$\psi(g(\omega)) = \frac{1}{g(\omega)}$ such that $g(\omega) \neq 0$. Then, since $g: \Omega \rightarrow \mathbb{R}$ and $g(\omega) \neq 0$, $\psi: \mathbb{R} \rightarrow \mathbb{R}$ is continuous. As such 




Let $A \in \mathcal{B}(\mathbb{R})$ and 

$$
\begin{aligned}
h^{-1}(A) &= \left\{\omega : \frac{f(\omega)}{g(\omega)} I(g(\omega) \neq 0) \in A\right\}\\[10pt]
f^{-1}(B) &= \left\{\omega : a \in A, g(\omega) \neq 0, ag(\omega) \in B \right\}
\end{aligned}
$$

&nbsp;&nbsp;&nbsp;&nbsp; Now let

$$
C = \left\{\frac{b}{a} : b\in B \setminus 0, a \in A \setminus 0 \right\} 
= \bigcup_{b \in B \setminus 0} \left\{ b \frac{1}{a} : a \in A \setminus 0 \right\}.
$$

Recall that $A \in \mathcal{B}(\mathbb{R})$, then so is $A \setminus 0$. Therefore, $A \setminus 0$ is the union of open, closed, and/or half-open intervals. Then, since every element in $A \setminus 0$ has a unique inverse in $\mathbb{R}$, $A^{-1} \equiv \left\{\frac{1}{a}\right\}_{a \in A \setminus 0} \in \mathcal{B}(\mathbb{R})$. 

&nbsp;&nbsp;&nbsp;&nbsp; Next, realize that for fixed $b$, $\left\{ b \frac{1}{a} : a \in A\right\}$ is cleary in $\mathcal{B}(\mathbb{R})$. Moreover, the arbitrary union of such $\left\{ b \frac{1}{a} : a \in A\right\}$ i.e., $\bigcup_{b \in B \setminus 0} \left\{ b \frac{1}{a} : a \in A\right\}$ is clearly a union of open, closed, and/or half-open subsets of $\mathbb{R}$ that is also in $\mathcal{B}(\mathbb{R})$. In other words, $C \in \mathcal{B}(\mathbb{R})$.

&nbsp;&nbsp;&nbsp;&nbsp; Since $C \in \mathcal{B}(\mathbb{R})$,

$$
g^{-1}(C) = \left\{\omega : g(\omega) \in C \right\} \in \mathcal{F}.
$$

Then, using similar arguments as above,

$$
\bigcup_{c \in C} \bigcup_{a \in A} \{ac\} = B \in \mathcal{B}(\mathbb{R})
$$

$$
\begin{aligned}
h^{-1}(A) &= \left\{\omega : \frac{f(\omega)}{g(\omega)} I(g(\omega) \neq 0) \in A\right\}\\[10pt]
f^{-1}(B) &= \left\{\omega : a \in A, g(\omega) \neq 0, ag(\omega) \in B \right\}\\[10pt]
\end{aligned}
$$

$$
g^{-1}(C) = \left\{ \omega : g(\omega) \in C \right\}
$$




$$
$$

Since $g$ is $\langle \mathcal{F}, \mathcal{B}(\mathbb{R}) \rangle$-measurable, 


 Moreover, since every set in $\mathbb{R}$

So, $C$ amounts to a union of elements in $\mathcal{B}(\mathbb{R})$, and since the union of 

 \in \mathcal{B}(\mathbb{R})

Since $A \in \mathcal{B}(\mathbb{R})$,

$\mathbb{R}$ are unique (i.e., there is a one-to-one relationship between A and $\left\{\frac{1}{a}\right\}_{a \in A}$) so 

Since $g$ is $\langle \mathcal{F}, \mathcal{B}(\mathbb{R}) \rangle$-measurable, 



$$
A = \left\{a \in \mathbb{R}: \right\}
$$

Since $f$ and $g$ are $\langle \mathcal{F}, \mathcal{B}(\mathbb{R}) \rangle$-measurable, we have that 

$$
\begin{aligned}
f^{-1}(B) = \left\{\omega : f(\omega) \in B \right\} \in \mathcal{F} && \text{for every } B \in \mathcal{B}(\mathbb{R}) 
\end{aligned}
$$

and

$$
\begin{aligned}
g^{-1}(C) = \left\{\omega : g(\omega) \in C \right\} \in \mathcal{F} && \text{for every } C \in \mathcal{B}(\mathbb{R}).
\end{aligned}
$$

Therefore, we have that for $B$ such that 

$$
f^{-1}(B)
= \left\{\omega : g(\omega) \neq 0, f(\omega) = h(\omega) g(\omega) \in B\right\} \in \mathcal{F}
$$


We want to show that for every $A \in \mathcal{B}(\mathbb{R})$ we have

$$
h^{-1}(A)
= \left\{\omega : h(\omega)  \in A \right\} 
= \left\{\omega :\frac{f(\omega)}{g(\omega)} I(g(\omega) \neq 0) \in A \right\}
\in \mathcal{F}.
$$

In other words, 
s
$$
h^{-1}(A)
= \left\{\omega : g(\omega) \neq 0, f(\omega) = h(\omega) g(\omega) \in B\right\} \in \mathcal{F}
$$

or

$$
h^{-1}(A)
= \left\{\omega : g(\omega) \neq 0, g(\omega) = \frac{f(\omega)}{h(\omega)} \in C\right\} \in \mathcal{F}
$$

and since we know 

# Problem 2.6

## (a)

## (b)

## (c)


# Extra Problems

## (A)



## (B)